///constant region
import cors from 'cors'
import express from 'express'
import jwt from 'express-jwt'
import helmet from 'helmet'
import path from 'path'
import SongActions from './routes/SongActions'
import UserActions from './routes/UserActions'

const app = express()
const port = process.env.PORT || 3001
const DIST_DIR = path.join(__dirname, '../dist') // NEW

//route config region
app.use(express.json({ limit: '50mb' })) // support json encoded bodies
app.use(express.urlencoded({ limit: '50mb', extended: true })) // support encoded bodies
app.use(express.static(DIST_DIR)) // NEW
app.use(helmet.xssFilter())
app.use(helmet.hidePoweredBy())
app.use(helmet.noSniff())
app.use(
  cors({
    origin: 'http://localhost:3000',
    methods: 'GET,HEAD,PUT,PATCH,POST,DELETE',
    preflightContinue: false,
    optionsSuccessStatus: 204
  })
)
app.use(
  jwt({
    secret: process.env.SERVICE_TOKEN_SECRET,
    algorithms: ['HS256'],
    resultProperty: 'locals.user'
  }).unless({
    path: ['/auth/createUser', '/auth/validateUser']
  })
)

//route files
require('dotenv').config()
app.use('/api', SongActions)
app.use('/auth', UserActions)

//Process document
app.listen(port, function() {
  console.log('App listening on port: ' + port)
})
