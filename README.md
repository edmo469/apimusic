# Web Api Music #

Web api of music that allow read,create,edit,delete and add liked songs

* Version 1.0.0

## Before you begin ##

* Install docker in your machine
* Execute the command docker compose up to run the project
* The app run on port 80
* The app run a startup process to load public song in the data base also you can add a new user example(email:test@gmail.com, password: Ttde348h??)
* If you want review the api documentation you can use the swagger documentation on doc/swagger.yml and check it it on <https://editor.swagger.io>

### Recommended vscode extensions ###

* Docker
* Markdown Preview Enhanced
* Pettier - Code formatter
* Eslint
