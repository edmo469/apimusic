import express from 'express'
import { CreateUser, ValidateUserAccess, ValidToken } from '../controllers/UserController'

const router = express.Router()

router.post('/validateUser', async (req, res) => ValidateUserAccess(req, res))

router.post('/createUser', async (req, res) => CreateUser(req, res))

router.get('/validToken', async (req, res) => ValidToken(req, res))

export default router
