import express from 'express'
import {
  AddSong,
  DeleteMySong,
  EditMySong,
  FavoriteSong,
  GetAllFavoriteSongs,
  GetAllPublicSongs,
  GetMySongs
} from '../controllers/SongController'

const router = express.Router()

router.get('/getFavoriteSongs', async (req, res) => await GetAllFavoriteSongs(req, res))

router.get('/getAllSongs', async (req, res) => await GetAllPublicSongs(req, res))

router.get('/getMySongs', async (req, res) => await GetMySongs(req, res))

router.post('/registerSong', async (req, res) => await AddSong(req, res))

router.put('/editSong', async (req, res) => await EditMySong(req, res))

router.delete('/removeSong', async (req, res) => await DeleteMySong(req, res))

router.patch('/editFavoriteSong', async (req, res) => await FavoriteSong(req, res))

export default router
