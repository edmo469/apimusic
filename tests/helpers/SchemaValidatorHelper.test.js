import { ValidateSchemas } from '../../helpers/SchemaValidatorHelper'
import { res } from '../mocks/mockRequest'

describe('validate schema', () => {
  test('schema is valid', async () => {
    const mockSchema = () => true
    let response = ValidateSchemas(mockSchema, res)
    expect(response).toEqual(undefined)
  })
  test('schema is invalid', async () => {
    const mockSchema = () => ({
      errors: 'no valid'
    })
    let response = ValidateSchemas(mockSchema, res)
    expect(response).toEqual(undefined)
  })
})
