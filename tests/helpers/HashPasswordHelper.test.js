import { HashPasswordAsync, isValidPasswordAsync } from '../../helpers/HashPasswordHelper'

describe('validate hash password functions helpers', () => {
  const mockPassword = 'Tths34@3456'
  test('generate a valid hash password success', async () => {
    let expectResponse = '$2b$10$'
    let hashPassword = await HashPasswordAsync(mockPassword)
    expect(hashPassword.includes(expectResponse)).toEqual(true)
  })

  test('compare two passwords success', async () => {
    let hashPassword = '$2b$10$ALYZYcaYBJI6cbUdtkeiS.bXyov12lghXNoSYqE.ewh3ftIiecwce'
    let validPassword = await isValidPasswordAsync(mockPassword, hashPassword)
    expect(validPassword).toEqual(true)
  })
})
