const res = {
  locals: {
    user: {
      user_id: '7875fdf45dsf'
    }
  },
  send: data => ({
    data
  }),
  status: code => ({
    statusCode: code,
    send: data => ({
      data
    })
  })
}

export { res }
