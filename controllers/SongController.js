import { ERROR_GENERAL } from '../constants/SongsErrorMessages'
import { ValidateSchemas } from '../helpers/SchemaValidatorHelper'
import { ValidateAccessToResources } from '../helpers/validateAccess'
import {
  AddMySong,
  DeleteSong,
  EditSong,
  GetMyFavoriteSongs,
  GetPublicSongs,
  GetSong,
  GetSongs
} from '../repository/Song/SongRepository'
import {
  schemaEditSong,
  schemaSong,
  schemaSongDelete,
  schemaSongFavorite
} from '../schemas/SongSchema'

const getSkip = (page, rowsPage) => (Number(page) - 1) * Number(rowsPage)
const mapSongs = (songs, userId) =>
  songs.map(song => ({ ...song, isMyFavorite: song.favoriteSong.includes(userId) }))

const GetAllPublicSongs = async ({ query }, res) => {
  try {
    const { page, rowsPage = 20 } = query
    let songs = await GetPublicSongs(getSkip(page, rowsPage), rowsPage)
    songs = mapSongs(songs, res.locals.user.user_id)
    res.send(songs)
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const GetMySongs = async ({ query }, res) => {
  try {
    const { page, rowsPage = 20 } = query
    let songs = await GetSongs(res.locals.user.user_id, getSkip(page, rowsPage), rowsPage)
    songs = mapSongs(songs, res.locals.user.user_id)
    res.send()
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const GetAllFavoriteSongs = async ({ query }, res) => {
  try {
    const { page, rowsPage = 20 } = query
    res.send(await GetMyFavoriteSongs(res.locals.user.user_id, getSkip(page, rowsPage), rowsPage))
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const AddSong = async ({ body }, res) => {
  try {
    ValidateSchemas(schemaSong, body, res)
    let song = { ...body, owner: res.locals.user.user_id }
    res.send(await AddMySong(song))
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const EditMySong = async ({ body }, res) => {
  try {
    ValidateSchemas(schemaEditSong, body, res)
    let song = { id: body.id, owner: res.locals.user.user_id }
    ValidateAccessToResources(song, res)
    res.send(await EditSong(body, { _id: body.id }))
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const DeleteMySong = async ({ body }, res) => {
  try {
    ValidateSchemas(schemaSongDelete, body, res)
    let song = { id: body.id, owner: res.locals.user.user_id }
    ValidateAccessToResources(song, res)
    res.send(await DeleteSong({ _id: body.id }))
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const FavoriteSong = async ({ body }, res) => {
  try {
    ValidateSchemas(schemaSongFavorite, body, res)
    let song = await GetSong(body.id)
    if (body.action === 'Add') song.favoriteSong = [...song.favoriteSong, res.locals.user.user_id]
    else song.favoriteSong = song.favoriteSong.filter(sg => sg !== res.locals.user.user_id)
    await EditSong(song, { _id: body.id })
    res.send(song)
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

export {
  GetAllPublicSongs,
  AddSong,
  GetMySongs,
  EditMySong,
  DeleteMySong,
  FavoriteSong,
  GetAllFavoriteSongs
}
