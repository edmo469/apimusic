import { ERROR_GENERAL } from '../constants/SongsErrorMessages'
import { USER_EXIST, WRONG_EMAIL_OR_PASSWORD } from '../constants/UserValidationMessages'
import { HashPasswordAsync, isValidPasswordAsync } from '../helpers/HashPasswordHelper'
import { ValidateSchemas } from '../helpers/SchemaValidatorHelper'
import { GetToken } from '../helpers/TokenHelper'
import { CreateNewUser, GetUser, ValidateEmailNotExist } from '../repository/User/UserRepository'
import { schemaUser } from '../schemas/UserSchema'

const CreateUser = async ({ body }, res) => {
  try {
    ValidateSchemas(schemaUser, body, res)
    if (!(await ValidateEmailNotExist(body.email))) {
      let password = await HashPasswordAsync(body.password)
      let newUser = await CreateNewUser({ ...body, password, created: new Date() })
      res.send(newUser)
    } else {
      res.status(404).send(USER_EXIST)
    }
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const ValidateUserAccess = async ({ body }, res) => {
  try {
    ValidateSchemas(schemaUser, body, res)
    let userData = await GetUser({ email: body.email }).lean()
    if (isValidPasswordAsync(body.password, userData.password)) {
      res.send(GetToken(userData))
    } else {
      res.status(401).send(WRONG_EMAIL_OR_PASSWORD)
    }
  } catch (error) {
    res.status(500).send(ERROR_GENERAL)
  }
}

const ValidToken = async (req, res) => {
  res.send('valid')
}

export { CreateUser, ValidateUserAccess, ValidToken }
