import mongoose, { Types } from 'mongoose'
import { LoadTemplateSongs } from '../helpers/LoadBaseDataHelper'
require('dotenv').config()

let mongoAtlasUri = `mongodb://${process.env.MONGODB_SECRET_USER}:${process.env.MONGODB_SECRET_PASS}@${process.env.MONGO_DB_HOST}`

const options = {
  useNewUrlParser: true,
  useUnifiedTopology: true,
  autoIndex: false, // Don't build indexes
  serverSelectionTimeoutMS: 5000, // Keep trying to send operations for 5 seconds
  socketTimeoutMS: 45000, // Close sockets after 45 seconds of inactivity
  family: 4, // Use IPv4, skip trying IPv6
  connectTimeoutMS: 100000,
  dbName: process.env.DATA_BD
}
try {
  mongoose
    .connect(mongoAtlasUri, options)
    .then(async () => {
      //load data base template songs
      await LoadTemplateSongs()
      console.log('Mongoose is connected')
    })
    .catch(error => console.log(error))
} catch (error) {
  console.log(error)
}

const Schema = mongoose.Schema
const ObjectId = mongoose.ObjectId
export { Schema, ObjectId, mongoose, Types }
