import { User } from '../../models/User'

const ValidateEmailNotExist = email => User.exists({ email })
const CreateNewUser = user => User.create(user)
const GetUser = userFilter => User.findOne(userFilter)

export { ValidateEmailNotExist, CreateNewUser, GetUser }
