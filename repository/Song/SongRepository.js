import { Song } from '../../models/Song'

const GetPublicSongs = (skip, limit) => Song.find({ owner: null }, null, { skip, limit }).lean()
const GetSongs = (userId, skip, limit) => Song.find({ owner: userId }, null, { skip, limit }).lean()
const GetSong = _id => Song.findOne({ _id }).lean()
const GetMyFavoriteSongs = (userId, skip, limit) =>
  Song.find({ favoriteSong: { $in: [userId] } }, null, { skip, limit }).lean()
const AddSong = song => Song.create(song)
const AddMySong = song => Song.create(song)
const EditSong = (song, filter) => Song.updateOne(filter, song)
const DeleteSong = filter => Song.deleteOne(filter)
const ValidateIsOwner = filter => Song.exists(filter)

export {
  AddSong,
  GetPublicSongs,
  GetSongs,
  GetSong,
  EditSong,
  DeleteSong,
  ValidateIsOwner,
  GetMyFavoriteSongs,
  AddMySong
}
