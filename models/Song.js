import { mongoose, ObjectId, Schema } from '../repository/MongoDbConnection'

// Song schema
let Songs = new Schema({
  id: { type: ObjectId },
  name: { type: String, default: '' },
  released: { type: String, default: '' },
  created: { type: String, default: new Date() },
  owner: { type: String, required: false },
  imageUrl: { type: String, default: '' },
  favoriteSong: { type: Array, default: [] },
  description: { type: String, default: '' }
})

export const Song = mongoose.model('songs', Songs)
