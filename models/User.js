import { mongoose, ObjectId, Schema } from '../repository/MongoDbConnection'
// User schema
let Users = new Schema({
  id: { type: ObjectId },
  email: { type: String, default: '' },
  password: { type: String, default: '' },
  created: { type: String, default: '' },
  status: { type: String, default: 'enabled' }
})

export const User = mongoose.model('users', Users)
