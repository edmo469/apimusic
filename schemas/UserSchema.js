import ajv from '../helpers/SchemaConfigHelper'

const schemaUser = ajv.compile({
  additionalProperties: false,
  type: 'object',
  properties: {
    email: { type: 'string', format: 'email-format' },
    password: { type: 'string', format: 'password-format' }
  },
  required: ['email', 'password']
})

export { schemaUser }
