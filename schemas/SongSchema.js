import ajv from '../helpers/SchemaConfigHelper'

const schemaSong = ajv.compile({
  additionalProperties: false,
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' },
    released: { type: 'string', format: 'date' },
    created: { type: 'string', format: 'date' },
    owner: { type: 'string' },
    imageUrl: { type: 'string' },
    description: { type: 'string' }
  },
  required: ['name', 'released', 'description']
})

const schemaEditSong = ajv.compile({
  additionalProperties: false,
  type: 'object',
  properties: {
    id: { type: 'string' },
    name: { type: 'string' },
    released: { type: 'string', format: 'date' },
    imageUrl: { type: 'string' },
    description: { type: 'string' }
  },
  required: ['id']
})

const schemaSongFavorite = ajv.compile({
  additionalProperties: false,
  type: 'object',
  properties: {
    id: { type: 'string' },
    action: { type: 'string', pattern: '^Add$|^Remove$' }
  },
  required: ['id', 'action']
})

const schemaSongDelete = ajv.compile({
  additionalProperties: false,
  type: 'object',
  properties: {
    id: { type: 'string' }
  },
  required: ['id']
})

export { schemaSong, schemaSongFavorite, schemaSongDelete, schemaEditSong }
