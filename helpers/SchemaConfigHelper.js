import Ajv from 'ajv'
import addFormats from 'ajv-formats'
import { REX_EMAIL, REX_PASSWORD } from '../constants/Format'

const ajv = new Ajv({ removeAdditional: true, allErrors: true })
addFormats(ajv)
ajv.addFormat('email-format', REX_EMAIL)
ajv.addFormat('password-format', REX_PASSWORD)

export default ajv
