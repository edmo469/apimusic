import jwt from 'jsonwebtoken'

const GetToken = ({ _id, email }) => {
  try {
    return jwt.sign({ user_id: _id, email }, process.env.SERVICE_TOKEN_SECRET, {
      expiresIn: process.env.EXPIRATION_TOKEN_TIME_MS
    })
  } catch (error) {
    return false
  }
}

export { GetToken }
