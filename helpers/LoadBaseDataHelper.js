import fs from 'fs'
import { AddMySong, GetPublicSongs } from '../repository/Song/SongRepository'

const LoadTemplateSongs = async () => {
  try {
    let songs = await GetPublicSongs()
    if (songs.length === 0) {
      let songs = JSON.parse(fs.readFileSync('templates/publicSongs.json'))
      songs = songs.map(song => ({ ...song, created: new Date() }))
      await AddMySong(songs)
      console.log('data songs loaded!')
    } else {
      console.log('data songs loaded before!')
    }
  } catch (error) {
    console.log(error)
  }
}

export { LoadTemplateSongs }
