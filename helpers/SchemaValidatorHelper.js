const ValidateSchemas = (schema, data, res) => {
  if (!schema(data)) res.status(404).send(schema.errors)
}
export { ValidateSchemas }
