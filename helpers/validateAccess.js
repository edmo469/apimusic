import { ACCESS_NOT_ALLOWED } from '../constants/SongsErrorMessages'
import { ValidateIsOwner } from '../repository/Song/SongRepository'

const ValidateAccessToResources = ({ id, owner }, res) => {
  if (!ValidateIsOwner({ _id: id, owner })) res.status(403).send(ACCESS_NOT_ALLOWED)
}

export { ValidateAccessToResources }
