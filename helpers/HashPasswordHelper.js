import bcrypt from 'bcrypt'

const HashPasswordAsync = async password => {
  const salt = await bcrypt.genSalt()
  const hash = await bcrypt.hash(password, salt)
  return hash
}

const isValidPasswordAsync = (password, hash) => {
  return bcrypt.compareSync(password, hash)
}

export { HashPasswordAsync, isValidPasswordAsync }
