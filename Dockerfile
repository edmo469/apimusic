FROM node:14.17.3

RUN useradd -ms /bin/bash pptruser

WORKDIR /app

COPY package.json ./
COPY package-lock.json ./

RUN npm install 

RUN rm -rf dist

COPY . .

RUN npm run build

CMD [ "npm", "start" ]
