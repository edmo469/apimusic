const ERROR_GENERAL = 'one or more problems during the request. please try again.'
const ACCESS_NOT_ALLOWED = 'you can get,modify and delete the song provided'

export { ERROR_GENERAL, ACCESS_NOT_ALLOWED }
