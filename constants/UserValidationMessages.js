const NOT_VALID_EMAIL = 'The email provided is not valid!'
const NOT_VALID_PASSWORD_ADDED = 'The password provided is not valid with the rules!'
const USER_EXIST = 'The provided email exist already!'
const WRONG_EMAIL_OR_PASSWORD = 'Email or password invalid!'

export { NOT_VALID_EMAIL, NOT_VALID_PASSWORD_ADDED, USER_EXIST, WRONG_EMAIL_OR_PASSWORD }
