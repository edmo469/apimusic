const path = require('path')

module.exports = {
  roots: ['<rootDir>/tests'],
  testRegex: '(.*\\.test\\.(js?))$',
  transform: {
    '^.+\\.js?$': 'babel-jest'
  },
  moduleFileExtensions: ['js', 'json'],
  moduleDirectories: ['node_modules', path.resolve(__dirname, 'src'), './'],
  globals: {
    TEST_MODE: true
  },
  clearMocks: true,
  resetMocks: true,
  restoreMocks: true,
  collectCoverage: true,
  coveragePathIgnorePatterns: ['index.js'],
  coverageThreshold: {
    global: {
      statements: 99.89,
      branches: 99.26,
      functions: 99.69,
      lines: 99.89
    }
  },
  setupFilesAfterEnv: ['./tests/jest.setup.js']
}
